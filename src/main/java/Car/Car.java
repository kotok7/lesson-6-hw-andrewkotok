package Car;

public class Car {

    public void start(){
        startElectrcity();
        startCommand();
        startFuelSystem();
    }

    private void startFuelSystem() {
        System.out.println("Fuel system started!");
    }

    private void startCommand() {
        System.out.println("Command system started!");
    }

    private void startElectrcity() {
        System.out.println("Electricity started!");
    }
}
