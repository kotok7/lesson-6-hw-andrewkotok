package Employee;

public class Main {
    public static void main(String[] args) {
        Employee man = new Employee("Andrew", "Johnson", "Nikolaevich", "Boss", "johnson@gmail.com", "+380977878784", 34);
        Employee woman = new Employee("Katherina", "Walker", "Sergeevna", "Boss'sHelper", "walker@gmail.com", "+380677878784", 33);
        System.out.println(man);
        System.out.println(woman);
    }
}
